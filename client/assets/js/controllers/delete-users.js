console.log(window.parent.location);

let params = new URLSearchParams(window.location.search)

let userId = params.get('userId');

let cardTitle = document.querySelector('.card-title')
let cardText = document.querySelector('.card-text')
let cardEmail = document.querySelector('.email')
let cardDesignation = document.querySelector('.designation')
let cardTimeStamp = document.querySelector('.time-stamps')

fetch('https://quiet-crag-52780.herokuapp.com/api/users/')
.then(response =>response.json())
.then(data=>{
	// console.log(data)
	let specificUser = data.map(user=>{
		if (user._id == userId){
			// console.log(user)
			cardTitle.innerHTML = `First Name: ${user.firstName}`
			cardText.innerHTML = `Last Name: ${user.lastName}`
			cardEmail.innerHTML = `Email: ${user.email}`
			cardDesignation.innerHTML = `Designation: ${user.isAdmin}`
		}
	})

})

document.getElementById('deleteUser').addEventListener('submit', (e)=>{
	e.preventDefault();

	fetch(`https://quiet-crag-52780.herokuapp.com/api/users/${userId}`,{
		method: 'DELETE',
		headers:{
			'Content-Type': 'application/json'
		}
	})
	.then(response=> response.json())
	.then(data=>{
		if (data==true){
			alert('User successfully deleted')
			window.location.replace('./usersList.html')
		} else {
			alert('Something went wrong!')
		}
	})
})