let loggedInCredentials = localStorage.getItem("id")
let id = loggedInCredentials

if (!loggedInCredentials || loggedInCredentials ==null){
	alert('Login first!')
	window.location.replace('../index.html')
} else {
	let x = id
 
	fetch(`https://quiet-crag-52780.herokuapp.com/api/users/${x}`)
	.then(response =>response.json())
	.then(data=>{
		let userNow = `
		<label>First Name:</label>
		<span class="userInfo">${data.firstName}</span>
		<br>
		<label>Last Name:</label>
		<span class="userInfo">${data.lastName}</span>
		<br>
		<label>Email:</label>
		<span class="userInfo">${data.email}</span>
		<br>
		<label>Password:</label>
		<span class="userInfo">${data.password}</span>
		<br>
		<label>Designation:</label>
		<span class="userInfo">${data.isAdmin}</span>
		<br>
		<label>Mobile Number:</label>
		<span class="userInfo">${data.mobileNo}</span>
		<br>
		<label>Course:</label>
		<span class="userInfo">${data.courseId}</span>
		<br>
		<label>Date Enrolled:</label>
		<span class="userInfo">${data.enrollmentDate}</span>
		<br>
		<label>Status:</label>
		<span class="userInfo">${data.status}</span>
		<br>
		`

		console.log(userNow)
		document.getElementById('information').innerHTML = userNow
		
	}) 
		logoutForm.addEventListener('submit', (e)=>{
		localStorage.clear()
	})
} 
