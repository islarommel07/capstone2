let loggedInCredentials = localStorage.getItem("id");

if(!loggedInCredentials || loggedInCredentials == null){
	let loginForm = document.getElementById('loginForm')

	loginForm.addEventListener('submit', (e)=>{
		e.preventDefault()

		let userName = document.getElementById('userName').value;
		let password = document.getElementById('password').value;

		if(userName == "" || password == "") {
			alert("Please input your email and password")
		} 
		else {
			fetch('https://quiet-crag-52780.herokuapp.com/api/users/login',{
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					userName : userName,
					password : password
				})
			})
			.then(res =>{
				return res.json()
			})
			.then(data =>{
				if (data.userLogin){
					// console.log(data.userLogin._id)

					localStorage.setItem("id", data.userLogin._id)
					localStorage.setItem("userName", data.userLogin.userName)
					localStorage.setItem("password", data.userLogin.password)

					window.location.replace("./pages/info.html")
				} 
				 else {
					alert("Ooops! Something went wrong!")
				}
			})
		}
	})
} 
else {
	window.location.replace('./pages/info.html')
}

