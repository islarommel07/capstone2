let firstName = document.getElementById('inputFirstName');
let lastName = document.getElementById('inputLastName');
let email = document.getElementById('inputEmail');
let password = document.getElementById('inputPassword');
let isAdmin = document.getElementById('inputIsAdmin');
let mobileNo = document.getElementById('inputMobileNo');
let courseId = document.getElementById('inputCourseId');
let enrollmentDate = document.getElementById('inputEnrollmentDate').value;
let status = document.getElementById('inputStatus');
let register = document.getElementById('register');

// let object = {firstName:"Rommel", lastName:"Isla", email:"email@mymail.com",password:"1234",designation:"Admin",mobileNo:"091599224",courseId:"bsit",enrollmentDate:"1/10/2020",status:"Enrolled"}

// console.log(object);
// console.log(JSON.stringify(object))

register.addEventListener('submit', (e) =>{
	e.preventDefault()
	let userFirstName = firstName.value;	
	let userLastName = lastName.value	
	let userEmail = email.value
	let userPassword = password.value
	let userIsAdmin = isAdmin.value
	let userMobileNo = mobileNo.value
	let userCourseId = courseId.value
	let userEnrollmentDate = enrollmentDate.value
	let userStatus = status.value

	fetch('https://quiet-crag-52780.herokuapp.com/api/users/',{
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			firstName	: userFirstName,
			lastName	: userLastName,
			email	: userEmail,
			password	: userPassword,
			isAdmin	: userIsAdmin,
			mobileNo	: userMobileNo,
			courseId	: userCourseId,
			enrollmentDate	: userEnrollmentDate,
			status	: userStatus
		})
	})
	.then(response =>response.json())
	.then(data=>{
		console.log(data);
		window.location.replace('./info.html')
	})	

})