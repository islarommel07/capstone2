let loggedInCredentials = localStorage.getItem("id");


if (!loggedInCredentials || loggedInCredentials == null){
	alert("Login first!")
	window.location.replace('../index.html')
} else {
	let userName = localStorage.getItem("id")

	document.getElementById('ann').innerHTML = "Client Update Form"

	let firstName = document.getElementById('inputFirstName')
	let lastName= document.getElementById('inputLastName')
	let email= document.getElementById('inputEmail')
	let password= document.getElementById('inputPassword')
	let isAdmin= document.getElementById('inputIsAdmin')
	let mobileNo= document.getElementById('inputMobileNo')
	let courseID= document.getElementById('inputCourseId')
	let enrollmentDate= document.getElementById('inputEnrollmentDate')
	let status= document.getElementById('inputStatus')
	let userUpdate = document.getElementById('register')

	userUpdate.addEventListener('submit', (e)=>{
		e.preventDefault()

		window.location.replace('./info.html')

		let updatedFirstName = firstName.value
		let updatedLastName = lastName.value
		let updatedEmail = email.value
		let updatedPassword = password.value
		let updatedIsAdmin = isAdmin.value
		let updatedMobileNo = mobileNo.value
		let updatedCourseId = courseID.value
		let updatedEnrollmentDate = enrollmentDate.value
		let updatedStatus = status.value

		let x= userName

		fetch(`https://quiet-crag-52780.herokuapp.com/api/users/${x}`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: updatedFirstName,
				lastName: updatedLastName,
				email: updatedEmail,
				password: updatedPassword,
				isAdmin: updatedIsAdmin,
				mobileNo: updatedMobileNo,
				courseID:updatedCourseId,
				enrollmentDate: updatedEnrollmentDate,
				status:updatedStatus
			})
		})
		.then(response => response.json())
		.then(data =>{
			console.log(data)
		})
	}) 

	let logoutForm = document.getElementById('logoutForm')

	logoutForm.addEventListener('submit', (e)=>{
		localStorage.clear()
	})
}  