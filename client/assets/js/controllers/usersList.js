let usersListContainer = document.getElementById('retrieveUsers');

fetch('https://quiet-crag-52780.herokuapp.com/api/users/')
.then(response =>response.json())
.then(data=>{
	let usersData;

	if(data.length<1){
		usersData = "No users created yet."
	} else {
		usersData = data.map(user =>{
			let userListItem = `
			<li class="list-group-item">
				<a href="delete-users.html?userId=${user._id}" data-userid="${user._id}">${user.firstName} ${user.lastName}</a>
			</li>
			`
			return userListItem
		}).join("")
	}
	console.log(usersData)
	usersListContainer.innerHTML = usersData
})